def read_matlab_file(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    import h5py

    try:
        d = loadmat(infile)
    except:
        d = dict(h5py.File(infile))


    return d
